#!/usr/bin/python

import random, os, time, sys

import datetime, random

os.system('su -c setenforce permissive')

ACTION_TYPE_KEY = 1
ACTION_TYPE_CMD = 2
ACTION_TYPE_TAP = 3
ACTION_TYPE_SWP = 4

TEAM_ORDER = [ [0 ,1 ,2], [1, 0, 2], [2, 0 ,1]]

TEAM_ORDER = TEAM_ORDER[ (datetime.datetime.now().day) % 3]

print( 'Todays order is ', TEAM_ORDER)

TEAM_CAPTAIN = TEAM_ORDER[0]

CMD_PREFIX = ''
if os.path.exists("/init.rc"):
    #in android
    CMD_PREFIX = ' su -c '
else:
    #in linux
    CMD_PREFIX = 'adb -s 4c82b8ad shell '
    CMD_PREFIX = 'adb -s e817fc05 shell '
print (CMD_PREFIX,"CMD_PREFIX")
GLOBAL_CUR_ACCOUNT = 0

ACTTIONS = {
    
    'KEYCODE_APP_SWITCH':[ACTION_TYPE_KEY, 1, 'KEYCODE_APP_SWITCH'],
    'KEYCODE_HOME'         :[ACTION_TYPE_KEY, 1, 'KEYCODE_HOME'],
    'KEYCODE_BACK'         :[ACTION_TYPE_KEY, 1, 'KEYCODE_BACK'],

    'CMD_START_VA_APP'    :[ACTION_TYPE_CMD, 5, ' am start -n io.va.exposed/io.virtualapp.splash.SplashActivity'],
    'CMD_REBOOT_VA_APP'    :[ACTION_TYPE_CMD, 5, ' am broadcast -a io.va.exposed.CMD --es cmd reboot'],

    'CMD_START_WD_0'     :[ACTION_TYPE_CMD, 5, ' am broadcast -a io.va.exposed.CMD --es cmd launch --es pkg com.gbits.atm.neice --es uid 0'],
    'CMD_START_WD_1'     :[ACTION_TYPE_CMD, 5, ' am broadcast -a io.va.exposed.CMD --es cmd launch --es pkg com.gbits.atm.neice --es uid 1'],
    'CMD_START_WD_2'     :[ACTION_TYPE_CMD, 5, ' am broadcast -a io.va.exposed.CMD --es cmd launch --es pkg com.gbits.atm.neice --es uid 2'],
    'CMD_START_WD_3'     :[ACTION_TYPE_CMD, 5, ' am broadcast -a io.va.exposed.CMD --es cmd launch --es pkg com.gbits.atm.neice --es uid 3'],

    # 'COOR_WD_PASS_VIDEO'         :[ACTION_TYPE_TAP,15, [280,650]],
    # 'COOR_WD_LOGIN'             :[ACTION_TYPE_TAP,15, [1260,1120]],
    # 'COOR_WD_AGREE'             :[ACTION_TYPE_TAP, 3, [1544,928]],
    # 'COOR_WD_TEAM_OPEN'         :[ACTION_TYPE_TAP, 3, [2460,285]],
    # 'COOR_WD_TEAM_CREATE'         :[ACTION_TYPE_TAP, 3, [530,1280]],
    # 'COOR_WD_FRIEND_OPEN'         :[ACTION_TYPE_TAP, 3, [85,1100]],
    # 'COOR_WD_FRIEND_TAB'        :[ACTION_TYPE_TAP, 3,[105,260]],
    # 'COOR_WD_FRIEND_OPEN_GROUP' :[ACTION_TYPE_TAP, 3, [590,230]],
    # 'COOR_WD_FRIEND_INVITE_OPEN':[ACTION_TYPE_TAP, 3, [1050,410]],
    # 'COOR_WD_FRIEND_INVITE_JOIN':[ACTION_TYPE_TAP, 3, [1612,1037]],
    # 'COOR_WD_TEAM_APPLYS'         :[ACTION_TYPE_TAP, 4, [1130,196]],
    # 'COOR_WD_TEAM_APPLYS_AGREE'    :[ACTION_TYPE_TAP, 2, [1120,520]],
    # 'COOR_WD_TASK_MAIN'            :[ACTION_TYPE_TAP, 4, [800,140]],
    # 'COOR_WD_TASK_BTN_TTT'        :[ACTION_TYPE_TAP, 2, [2110,420]],
    # 'COOR_WD_YOUCE_BTN1'        :[ACTION_TYPE_TAP, 9, [2200,1000]],
    # 'COOR_WD_YOUCE_BTN2'        :[ACTION_TYPE_TAP, 3, [2200,1150]],
    # 'COOR_WD_YOUCE_BTN3'        :[ACTION_TYPE_TAP, 3, [2200,1300]],
    # 'COOR_TTT_CHOICE_EXP'        :[ACTION_TYPE_TAP, 2, [1080,820]],
    # 'COOR_TTT_GOFRIGHT'            :[ACTION_TYPE_TAP, 3, [2320,620]],

    # 'COOR_WD_RW_L1'                :[ACTION_TYPE_TAP,15, [1322,232-10]],
    # 'COOR_WD_RW_L2'                :[ACTION_TYPE_TAP,15, [1322,465-10]],
    # 'COOR_WD_RW_L3'                :[ACTION_TYPE_TAP,15, [1322,698-10]],
    # 'COOR_WD_RW_L4'                :[ACTION_TYPE_TAP,15, [1322,931-10]],
    # 'COOR_WD_RW_R1'                :[ACTION_TYPE_TAP,15, [2100,232-10]],
    # 'COOR_WD_RW_R2'                :[ACTION_TYPE_TAP,15, [2100,465-10]],
    # 'COOR_WD_RW_R3'                :[ACTION_TYPE_TAP,15, [2100,698-10]],
    # 'COOR_WD_RW_R4'                :[ACTION_TYPE_TAP,15, [2100,931-10]],

    # 'COOR_WD_BTN_SHUADAO'          :[ACTION_TYPE_TAP, 4, [677,140]],
    # 'COOR_WD_SHUADAO_FM'           :[ACTION_TYPE_TAP,14, [1300,960]],
    # 'COOR_WD_SHUADAO_PIPEI'        :[ACTION_TYPE_TAP, 5, [1550,1277]],

    # 'COOR_WD_MALL'                 :[ACTION_TYPE_TAP, 4, [75, 430]],
    # 'COOR_WD_MALL_TAB_VIP'         :[ACTION_TYPE_TAP, 4, [2290, 515]],
    # 'COOR_WD_MALL_TAB_VIP_100'     :[ACTprocessAction('COOR_WD_RW_JY')ION_TYPE_TAP, 4, [1920,1290]],
    # 'COOR_WD_FULI'                 :[ACTION_TYPE_TAP, 4, [75, 730]],
    # 'COOR_WD_FULI_11'              :[ACTION_TYPE_TAP, 4, [1030, 400]],
    # 'COOR_WD_FULI_45'              :[ACTION_TYPE_TAP, 4, [2030,1090]],
    # 'COOR_WD_FULI_END'             :[ACTION_TYPE_TAP, 4, [2030,1145]],

    # 'COOR_WD_YESNO_YES'           :[ACTION_TYPE_TAP, 3, [1490,850]],
    # 'COOR_WD_QUEDING'             :[ACTION_TYPE_TAP, 3, [1280,860]],
    # 'COOR_WD_GG_CLOSE'            :[ACTION_TYPE_TAP, 3, [2188,130]],
    # 'COOR_WD_TEAM_MEMBER1'        :[ACTION_TYPE_TAP, 3, [890, 790]],
    # 'COOR_WD_TEAM_MEMBER2'        :[ACTION_TYPE_TAP, 3, [1280,790]],
    # 'COOR_WD_TEAM_MEMBER1_DUIZHANG' :[ACTION_TYPE_TAP, 3, [1316,1016]],
    # 'COOR_WD_TEAM_MEMBER2_DUIZHANG' :[ACTION_TYPE_TAP, 3, [ 885,1016]],




    # 'COOR_WD_JJC_AI'              :[ACTION_TYPE_TAP, 3, [590,950]],
    # 'COOR_WD_JJC_OK'              :[ACTION_TYPE_TAP, 3, [1490,850]],
    # 'COOR_WD_ZIDONGZHANDOU'       :[ACTION_TYPE_TAP, 5, [2450,1330]],
    # 'COOR_WD_TASK_BTN_10'         :[ACTION_TYPE_TAP,15, []],
    # 'COOR_WD_BTN_XUNLUO'          :[ACTION_TYPE_TAP, 4, [520,75]],
    # 'COOR_XL_RENWU_JINENG'        :[ACTION_TYPE_TAP, 2, [1070,1130]],
    # 'COOR_XL_RW_JN_M5'            :[ACTION_TYPE_TAP, 2, [2080,630]],
    # 'COOR_XL_RW_JN_Z5'            :[ACTION_TYPE_TAP, 2, [2080,450]],
    # 'COOR_XL_RW_JN_F5'            :[ACTION_TYPE_TAP, 2, [2080,270]],
    # 'COOR_XL_RW_JN_M3'            :[ACTION_TYPE_TAP, 2, [1715,630]],
    # 'COOR_XL_RW_JN_Z3'            :[ACTION_TYPE_TAP, 2, [1715,450]],
    # 'COOR_XL_RW_JN_F3'            :[ACTION_TYPE_TAP, 2, [1715,270]],
    # 'COOR_XL_BAOBAO_JINENG'       :[ACTION_TYPE_TAP, 2, [1800,1130]],
    # 'COOR_XL_BB_JN_M2'            :[ACTION_TYPE_TAP, 2, [1540,640]], #1550 450
    # 'COOR_XL_BB_JN_M2_MU'         :[ACTION_TYPE_TAP, 2, [1540,450]], #1550 450
    # 'COOR_XL_RW_ZUHE'             :[ACTION_TYPE_TAP, 2, [1366,1015]],
    # 'COOR_WD_CLOSE_MIANBAN'       :[ACTION_TYPE_TAP, 2, [128,950]],
    # 'COOR_WD_CLOSE_MIANBAN_R'     :[ACTION_TYPE_TAP, 2, [2150,60]],
    # 'COOR_WD_CLOSE_FRIEND'        :[ACTION_TYPE_TAP, 2, [1210,700]],
    # 'COOR_WD_TASK_SWP_END'        :[ACTION_TYPE_SWP, 2, [1440,950],[1440,10]]

    ####################################################################################
    'COOR_WD_PASS_VIDEO': [ACTION_TYPE_TAP, 15, [280, 650]],#
    'COOR_WD_LOGIN': [ACTION_TYPE_TAP, 15, [962, 850]],#
    'COOR_WD_AGREE': [ACTION_TYPE_TAP, 3, [1158, 695]],#
    'COOR_WD_TEAM_OPEN': [ACTION_TYPE_TAP, 3, [1829, 214]],#
    'COOR_WD_TEAM_CREATE': [ACTION_TYPE_TAP, 3, [393,964]],#
    'COOR_WD_FRIEND_OPEN': [ACTION_TYPE_TAP, 3, [61, 816]],#
    'COOR_WD_FRIEND_TAB': [ACTION_TYPE_TAP, 3, [72, 195]],#
    'COOR_WD_FRIEND_OPEN_GROUP': [ACTION_TYPE_TAP, 3, [470, 170]],#
    'COOR_WD_FRIEND_INVITE_OPEN': [ACTION_TYPE_TAP, 3, [786, 306]],#
    'COOR_WD_FRIEND_INVITE_JOIN': [ACTION_TYPE_TAP, 3, [1217, 785]],#
    'COOR_WD_TEAM_APPLYS': [ACTION_TYPE_TAP, 4, [836, 150]],#
    'COOR_WD_TEAM_APPLYS_AGREE': [ACTION_TYPE_TAP, 2, [840, 386]],#
    'COOR_WD_TASK_MAIN': [ACTION_TYPE_TAP, 4, [622, 49]],
    'COOR_WD_TASK_BTN_TTT': [ACTION_TYPE_TAP, 2, [2110, 420]],
    'COOR_WD_YOUCE_BTN1': [ACTION_TYPE_TAP, 9, [1516, 750]],#
    'COOR_WD_YOUCE_BTN2': [ACTION_TYPE_TAP, 3, [1516, 858]],#
    'COOR_WD_YOUCE_BTN3': [ACTION_TYPE_TAP, 3, [1516, 966]],#
    'COOR_TTT_CHOICE_EXP': [ACTION_TYPE_TAP, 2, [802, 530]],
    'COOR_TTT_GOFRIGHT': [ACTION_TYPE_TAP, 3, [1720, 475]],
    'COOR_TTT_CLOSE_CANCEL': [ACTION_TYPE_TAP, 3, [960, 787]],
    'COOR_TTT_LEAVE'   : [ACTION_TYPE_TAP, 3, [1820, 595]],
    'COOR_WD_RW_L1': [ACTION_TYPE_TAP, 15, [992.1, 250.1]],#178
    'COOR_WD_RW_L2': [ACTION_TYPE_TAP, 15, [992.1, 420.1]],#350
    'COOR_WD_RW_L3': [ACTION_TYPE_TAP, 15, [992.1, 590.1]],#519
    'COOR_WD_RW_L4': [ACTION_TYPE_TAP, 15, [992.1, 740.1]],#686
    'COOR_WD_RW_R1': [ACTION_TYPE_TAP, 15, [1570.1, 250.1]],#
    'COOR_WD_RW_R2': [ACTION_TYPE_TAP, 15, [1570.1, 420.1]],#
    'COOR_WD_RW_R3': [ACTION_TYPE_TAP, 15, [1570.1, 590.1]],#
    'COOR_WD_RW_R4': [ACTION_TYPE_TAP, 15, [1570.1, 740.1]],#

    # 'COOR_WD_RW_R4': [ACTION_TYPE_TAP, 15, [1570.1, 740.1]],#

    'COOR_WD_RW_LR1':[ACTION_TYPE_TAP, 15, [992.1, 315.1]],#350
    'COOR_WD_RW_LR2':[ACTION_TYPE_TAP, 15, [992.1, 475.1]],#350
    'COOR_WD_RW_LR3':[ACTION_TYPE_TAP, 15, [992.1, 645.1]],#178

    'COOR_WD_RW_RR1': [ACTION_TYPE_TAP, 15, [1570.1, 315.1]],#
    'COOR_WD_RW_RR2': [ACTION_TYPE_TAP, 15, [1570.1, 475.1]],#
    'COOR_WD_RW_RR3': [ACTION_TYPE_TAP, 15, [1570.1, 645.1]],#

    'COOR_WD_RW_DJ' : [ACTION_TYPE_TAP, 2,  [1194.1, 140.1]],#任务选择上面的道具
    'COOR_WD_RW_JY' : [ACTION_TYPE_TAP, 2,  [770.1, 140.1]],#任务选择上面的经验
    'COOR_WD_RW_QB' : [ACTION_TYPE_TAP, 2,  [548.1 , 140.1]],#任务选择上面的全部

    'COOR_WD_BTN_SHUADAO': [ACTION_TYPE_TAP, 4, [513, 49]],#
    'COOR_WD_SHUADAO_FM': [ACTION_TYPE_TAP, 14, [975, 724]],#
    'COOR_WD_SHUADAO_PIPEI': [ACTION_TYPE_TAP, 5, [1165, 960]],#

    'COOR_WD_MALL': [ACTION_TYPE_TAP, 4, [56, 323]],#
    'COOR_WD_MALL_TAB_VIP': [ACTION_TYPE_TAP, 4, [1715, 380]],#
    'COOR_WD_MALL_TAB_VIP_100': [ACTION_TYPE_TAP, 4, [1464, 977]],#
    'COOR_WD_FULI': [ACTION_TYPE_TAP, 4, [56, 543]],#
    'COOR_WD_FULI_11': [ACTION_TYPE_TAP, 4, [770, 300]],#
    'COOR_WD_FULI_45': [ACTION_TYPE_TAP, 4, [1520, 820]],#
    'COOR_WD_FULI_END': [ACTION_TYPE_TAP, 4, [1520, 862]],#

    'COOR_WD_YESNO_YES': [ACTION_TYPE_TAP, 3, [1120, 643]],#COOR_WD_JJC_OK
    'COOR_GENGXIN_QUEDING':[ ACTION_TYPE_TAP, 3, [1120, 674]],
    'COOR_WD_QUEDING': [ACTION_TYPE_TAP, 3, [957, 641]],#
    'COOR_WD_GG_CLOSE': [ACTION_TYPE_TAP, 3, [1820, 640]],#
    'COOR_WD_TEAM_MEMBER1': [ACTION_TYPE_TAP, 3, [687, 586]],
    'COOR_WD_TEAM_MEMBER2': [ACTION_TYPE_TAP, 3, [972, 586]],
    'COOR_WD_TEAM_MEMBER1_DUIZHANG': [ACTION_TYPE_TAP, 3, [992, 764]],
    'COOR_WD_TEAM_MEMBER2_DUIZHANG': [ACTION_TYPE_TAP, 3, [660, 764]],




    'COOR_WD_JJC_AI': [ACTION_TYPE_TAP, 3, [440, 841]],#
    'COOR_WD_JJC_OK': [ACTION_TYPE_TAP, 3, [1120, 643]],#COOR_WD_YESNO_YES
    'COOR_WD_ZIDONGZHANDOU': [ACTION_TYPE_TAP, 5, [1839, 1008]],#
    'COOR_WD_TASK_BTN_10': [ACTION_TYPE_TAP, 15, []],
    'COOR_WD_BTN_XUNLUO': [ACTION_TYPE_TAP, 4, [395, 49]],
    'COOR_XL_RENWU_JINENG': [ACTION_TYPE_TAP, 2, [827, 846]],
    'COOR_XL_RW_JN_M5': [ACTION_TYPE_TAP, 2, [2080, 630]],
    'COOR_XL_RW_JN_Z5': [ACTION_TYPE_TAP, 2, [2080, 450]],
    'COOR_XL_RW_JN_F5': [ACTION_TYPE_TAP, 2, [2080, 270]],
    'COOR_XL_RW_JN_M3': [ACTION_TYPE_TAP, 2, [1715, 630]],
    'COOR_XL_RW_JN_Z3': [ACTION_TYPE_TAP, 2, [1715, 450]],
    'COOR_XL_RW_JN_F3': [ACTION_TYPE_TAP, 2, [1715, 270]],
    'COOR_XL_BAOBAO_JINENG': [ACTION_TYPE_TAP, 2, [1800, 1130]],
    'COOR_XL_BB_JN_M2': [ACTION_TYPE_TAP, 2, [1540, 640]],  # 1550 450
    'COOR_XL_BB_JN_M2_MU': [ACTION_TYPE_TAP, 2, [1540, 450]],  # 1550 450
    'COOR_XL_RW_ZUHE': [ACTION_TYPE_TAP, 2, [1366, 1015]],
    'COOR_WD_CLOSE_MIANBAN': [ACTION_TYPE_TAP, 2, [20, 685]],#
    'COOR_WD_CLOSE_MIANBAN_R': [ACTION_TYPE_TAP, 2, [1882, 37]],#
    'COOR_WD_CLOSE_FRIEND': [ACTION_TYPE_TAP, 2, [897.5, 521.5]],#
    'COOR_WD_TASK_SWP_END': [ACTION_TYPE_SWP, 2, [1096, 730], [1090, 10]]#
}



def doLockScreen():
    print( 'LockScreen' )
    # cmdstr = CMD_PREFIX + ' input keyevent KEYCODE_SLEEP'
    cmdstr = CMD_PREFIX + ' echo "0" > /sys/class/leds/lcd-backlight/brightness'
    os.system(cmdstr)

def doWakeUp():
    print( 'WakeUpScreen' )
    # cmdstr = CMD_PREFIX + ' input keyevent KEYCODE_WAKEUP'
    cmdstr = CMD_PREFIX + ' echo "30" > /sys/class/leds/lcd-backlight/brightness'
    os.system(cmdstr)

def doPower():
    cmdstr = CMD_PREFIX + ' input keyevent KEYCODE_POWER'
    os.system(cmdstr)

def doPrompt(str):
    pass

def randomCoordinate(x):
    xr = 0
    if isinstance(x, int):
        #is digital
        xr = int(random.random()*20) - 10
    return x+xr

def doTap(coord):
    x = randomCoordinate(coord[0])
    y = randomCoordinate(coord[1])
    cmdstr = CMD_PREFIX + ' input tap ' + str(x) + ' ' + str(y)
    os.system(cmdstr)


def doKey(keycode):
    cmdstr = CMD_PREFIX + ' input keyevent ' + keycode
    os.system(cmdstr)

def doCmd(cmd):
    cmdstr = CMD_PREFIX + cmd
    os.system(cmdstr)
def doSwp(coord1, coord2):
    cmdstr = CMD_PREFIX + " input swipe "+str(coord1[0])+" "+str(coord1[1])+" "+str(coord2[0])+" "+str(coord2[1])
    print( cmdstr )
    os.system(cmdstr)

def processAction(actStr,sleepTime = 0):
    actObj = ACTTIONS[actStr]
    if sleepTime == 0:
        sleepTime = actObj[1]
    print( 'Sleep',sleepTime," cmd:", actStr)
    if   actObj[0] == ACTION_TYPE_CMD:
        doCmd(actObj[2])
    elif actObj[0] == ACTION_TYPE_KEY:
        doKey(actObj[2])
    elif actObj[0] == ACTION_TYPE_TAP:
        doTap(actObj[2])
    elif actObj[0] == ACTION_TYPE_SWP:
        doSwp(actObj[2],actObj[3])
    else:
        print( 'Error: no action matched')
    
    doWait(sleepTime)


def doNotify(title="",msg=""):
    os.system('notify-send  "' + title + '" "' + msg+'"')


def doInitVA():
    processAction('CMD_START_VA_APP')
    processAction('CMD_REBOOT_VA_APP')


def doLogin(uid):
    doWakeUp()
    doNotify('doLogin')
    processAction('CMD_START_WD_'+str(uid))
    doWait(10)
    processAction('COOR_WD_PASS_VIDEO')
    doUpdate()
    processAction('COOR_WD_LOGIN')
    processAction('COOR_WD_AGREE')

def doTeamCreate():
    processAction('COOR_WD_CLOSE_MIANBAN')
    processAction('COOR_WD_TEAM_OPEN')
    processAction('COOR_WD_TEAM_CREATE')
    processAction('COOR_WD_CLOSE_MIANBAN')


def doTeamJoin():
    processAction('COOR_WD_CLOSE_MIANBAN')
    # processAction('COOR_WD_CLOSE_FRIEND')
    processAction('COOR_WD_AGREE')
    processAction('COOR_WD_FRIEND_OPEN')
    processAction('COOR_WD_FRIEND_TAB')
    processAction('COOR_WD_FRIEND_OPEN_GROUP')
    processAction('COOR_WD_FRIEND_INVITE_OPEN')
    processAction('COOR_WD_FRIEND_INVITE_JOIN')
    processAction('COOR_WD_FRIEND_OPEN_GROUP')
    processAction('COOR_WD_CLOSE_FRIEND')

def doTeamAgree():
    processAction('COOR_WD_AGREE')
    processAction('COOR_WD_TEAM_OPEN')
    processAction('COOR_WD_TEAM_APPLYS')
    processAction('COOR_WD_TEAM_APPLYS_AGREE')
    processAction('COOR_WD_TEAM_APPLYS_AGREE')
    processAction('COOR_WD_TEAM_APPLYS_AGREE')
    processAction('COOR_WD_CLOSE_MIANBAN')

def doWDLaunch(uid,sleepTime = 6):
    #uid = TEAM_ORDER[uid]
    processAction('CMD_START_WD_'+str(uid), sleepTime)

def doWait(tsec):
    try:
        for x in range(tsec):
            print( "\r", tsec,'/', tsec-x,"    ",)
            sys.stdout.flush()
            time.sleep(1)
    except KeyboardInterrupt:
        if tsec <= 7:
            exit()
        print( 'Sleep Canceled' )
    finally:
        print( '' )
        

def doChooseJineng(rwJineng, cwJineng = 'M2'):
    processAction('COOR_WD_CLOSE_MIANBAN')
    processAction('COOR_WD_CLOSE_MIANBAN')
    processAction('COOR_WD_BTN_XUNLUO')
    processAction('COOR_XL_RENWU_JINENG')
    if rwJineng == 'M3':
        processAction('COOR_XL_RW_JN_M3')
    elif rwJineng == 'M5':
        processAction('COOR_XL_RW_JN_M5')
    elif rwJineng == 'Z3':
        processAction('COOR_XL_RW_JN_Z3')
    elif rwJineng == 'Z5':
        processAction('COOR_XL_RW_JN_Z5')
    elif rwJineng == 'ZU':
        processAction('COOR_XL_RW_ZUHE')
    elif rwJineng == 'ZU_JIN':
        processAction('COOR_XL_RW_ZUHE')
    elif rwJineng == 'ZU_SHUI':
        processAction('COOR_XL_RW_ZUHE')
    elif rwJineng == 'ZU_MU':
        processAction('COOR_XL_RW_ZUHE')
    # elif rwJineng == 'ZU_F5_M5':
    #     processAction('COOR_XL_RW_JN_F5')
    #     processAction('COOR_XL_RW_JN_M5')
    # elif rwJineng == 'ZU_F5_M3':
    #     processAction('COOR_XL_RW_JN_F5')
    #     processAction('COOR_XL_RW_JN_M3')
    # elif rwJineng == 'ZU_F5_Z5':
    #     processAction('COOR_XL_RW_JN_F5')
    #     processAction('COOR_XL_RW_JN_Z5')
    processAction('COOR_XL_BAOBAO_JINENG')
    if cwJineng == 'M2':
        processAction('COOR_XL_BB_JN_M2')
    elif cwJineng == 'M2_MU':
        processAction('COOR_XL_BB_JN_M2_MU')
            
    processAction('COOR_WD_CLOSE_MIANBAN')


def doJinengTTT():
    # processAction('CMD_START_WD_0')
    # doWait(8)
    # doChooseJineng('M5')

    processAction('CMD_START_WD_1')
    doWait(8)
    doChooseJineng('ZU_SHUI')
    
    processAction('CMD_START_WD_2')
    doWait(8)
    doChooseJineng('Z3','M2_MU')

    doWDLaunch(0)

def doJinengRichang():
    # processAction('CMD_START_WD_0')
    # doWait(8)
    # doChooseJineng('M5')

    processAction('CMD_START_WD_1')
    doWait(8)
    doChooseJineng('M3')
    
    # processAction('CMD_START_WD_2')
    # doWait(8)
    # doChooseJineng('Z3','M2_MU')
    
    doWDLaunch(0)
    


def doTaskTTTSingle():
    doWakeUp()
    processAction('COOR_WD_CLOSE_MIANBAN')
    processAction('COOR_WD_TASK_MAIN')
    processAction('COOR_WD_RW_DJ')
    processAction('COOR_WD_RW_L2')
    doWait(15)
    processAction('COOR_WD_YOUCE_BTN1', 3)
    processAction('COOR_WD_YOUCE_BTN1', 3)
    # processAction('COOR_TTT_CHOICE_EXP',1)
    doWait(8)
    processAction('COOR_TTT_GOFRIGHT')
    doLockScreen()
    doWait(20*60)

def doSaoDang():
    doWakeUp()
    #baibangmang
    processAction('COOR_WD_CLOSE_MIANBAN')
    processAction('COOR_WD_TASK_MAIN')
    processAction('COOR_WD_RW_L4')
    # doWait(15)
    processAction('COOR_WD_YOUCE_BTN2', 2)#saodang
    processAction('COOR_WD_YOUCE_BTN1', 2)#jingyan
    processAction('COOR_WD_YOUCE_BTN2', 2)#yuanyou
    processAction('COOR_WD_CLOSE_MIANBAN')

    #xiufa
    processAction('COOR_WD_TASK_MAIN')
    processAction('COOR_WD_TASK_SWP_END')
    processAction('COOR_WD_RW_LR2')
    doWait(3)
    processAction('COOR_WD_YOUCE_BTN2', 2)#saodang
    processAction('COOR_WD_YOUCE_BTN2', 2)#yuanyou
    processAction('COOR_WD_CLOSE_MIANBAN')


    #bangpaitiaozhan
    processAction('COOR_WD_TASK_MAIN')
    processAction('COOR_WD_RW_L4')
    doWait(5)
    processAction('COOR_WD_YOUCE_BTN3', 2)  # saodang
    processAction('COOR_WD_CLOSE_MIANBAN')

def doUpdate():
    if datetime.datetime.now().weekday() == 4:
        print('need update')
        processAction("COOR_WD_YESNO_YES")
        doWait(30)
        processAction('COOR_WD_QUEDING')
        doWait(13)
        processAction('COOR_WD_GG_CLOSE')
    else:
        print('not need update')
        
def doTaskTTT2021():
    doWakeUp()
    doWDLaunch(0)
    doPreTaskMenu()
    processAction('COOR_WD_RW_JY')
    processAction('COOR_WD_RW_L4')
    doWait(15)

def doPreTaskMenu():
    doWait(5)
    processAction('COOR_WD_CLOSE_MIANBAN')
    processAction('COOR_WD_TASK_MAIN')



def doTaskTTT():
    doWakeUp()
    doWDLaunch(0)
    doWait(5)
    processAction('COOR_WD_CLOSE_MIANBAN')
    processAction('COOR_WD_TASK_MAIN')
    processAction('COOR_WD_RW_DJ')
    processAction('COOR_WD_RW_L2')
    doWait(4)
    processAction('COOR_WD_YOUCE_BTN1', 1)
    processAction('COOR_TTT_CHOICE_EXP',1)
    doWDLaunch(1)
    # doWait(2)
    processAction('COOR_TTT_CHOICE_EXP', 1)
    doWDLaunch(2)
    # doWait(2)
    processAction('COOR_TTT_CHOICE_EXP', 1)
    doWait(1)
    doWDLaunch(0)
    processAction('COOR_TTT_GOFRIGHT')




def doVIP100():
    processAction('COOR_WD_MALL')
    processAction('COOR_WD_MALL_TAB_VIP',2)
    processAction('COOR_WD_MALL_TAB_VIP_100',2)
    processAction('COOR_WD_CLOSE_MIANBAN')


def doQianDaoCoord(today = 0,month = 0):
    '''
    <= 30
    1-5  line 1
    6-10 line 2
    11-15 line 2
    16-20 line -3
    21-25 line -2
    26-30 line -1

    31
    1-5  line 1
    6-10 line 2
    11-15 line 2
    16-20 line 2
    21-25 line -3
    26-30 line -2
    31 line -1
    '''
    if today == 0:
        today = datetime.datetime.now().day
    else:
        today = int(today)
    # print 'Today ', today
    if month == 0:
        mon = datetime.datetime.now().month
    else:
        mon = int(month)

    if mon in [1,3,5,7,8,10,12]:
        monsplit = 19
    else:
        monsplit = 14
        if today > 30:
            today = 30
    # monsplit = 19
    
    today = today - 1
    line = today / 5
    column = today % 5

    coord = [0, 0]
    x0 = ACTTIONS['COOR_WD_FULI_11'][2][0]
    x1 = ACTTIONS['COOR_WD_FULI_45'][2][0]
    y0 = ACTTIONS['COOR_WD_FULI_11'][2][1]
    y1 = ACTTIONS['COOR_WD_FULI_45'][2][1]
    width = (x1 - x0) / 4
    height = (y1 - y0) / 3
    coord[0] = x0 + width * column
    if today < 5:
        coord[1] = y0 + height * 0
    elif today < 15 or (today < 20 and monsplit == 19):
        coord[1] = y0 + height * 1
    else:
        remainsday = today - monsplit -1
        rline = remainsday / 5
        # print 'rline',rline,remainsday,today,monsplit
        coorEnd = ACTTIONS['COOR_WD_FULI_END'][2]
        coord[1] = coorEnd[1] - (2 - rline)*height

    # print coord
    # doTap(coord)
    return coord

# alls = []
# print 'AAA start'
# for x in xrange(1,32):
#     alls.append( doQianDaoCoord(x,1) )

# for x in range(31):
#     if x % 5 == 0:
#         print ""
#     print alls[x],

# # # doQianDaoCoord(30,4)
# # doQianDaoCoord(31,1)


# exit()
# print doQianDaoCoord()

def doQianDao(today = 0):
    
    processAction('COOR_WD_FULI')
    doTap( doQianDaoCoord() )
    processAction('COOR_WD_CLOSE_MIANBAN')
    doVIP100()

    # if today == 0:
    #     today = datetime.datetime.now().day
    # else:
    #     today = int(today)
    # print 'Today ', today
    # today = today - 1
    # line = today / 5
    # column = today % 5

    # coord = [0, 0]

    # x0 = ACTTIONS['COOR_WD_FULI_11'][2][0]
    # x1 = ACTTIONS['COOR_WD_FULI_45'][2][0]
    # y0 = ACTTIONS['COOR_WD_FULI_11'][2][1]
    # y1 = ACTTIONS['COOR_WD_FULI_45'][2][1]
    # width = (x1 - x0) / 4
    # height = (y1 - y0) / 3

    # if line < 4:
    #     coord[0] = x0 + width * column
    #     coord[1] = y0 + height * line
    # else:
    #     coorEnd = ACTTIONS['COOR_WD_FULI_END'][2]
    #     doSwp(ACTTIONS['COOR_WD_TASK_SWP_END'][2][0],ACTTIONS['COOR_WD_TASK_SWP_END'][2][1])
    #     coord[0] = x0 + width * column
    #     coord[1] = coorEnd[1] - (5 - line)

    # doTap(coord)
    # processAction('COOR_WD_CLOSE_MIANBAN')
    # # 


def doTaskPanelRenwu(pos, btn='YOUCE_BTN1'):
    pos = pos.upper()
    doWakeUp()
    doWait(2)
    processAction('COOR_WD_TASK_MAIN')
    processAction('COOR_WD_TASK_MAIN')
    processAction('COOR_WD_RW_'+pos)
    if(btn != ''):
        processAction('COOR_WD_'+btn)
    # doLockScreen()

def doTaskPanelRenwuL4():
    doTaskPanelRenwu('L4')
    

def doShimen():
    doWakeUp()
    processAction('COOR_WD_CLOSE_MIANBAN')
    doNotify('doShimen')
    doWait(5)
    processAction('COOR_WD_TASK_MAIN')
    processAction('COOR_WD_TASK_MAIN')
    processAction('COOR_WD_RW_L1')
    doWait(5)
    processAction('COOR_WD_YOUCE_BTN1')
    doLockScreen()
    doWait(60*5)
    


def doBangpaiRenwu():
    doWakeUp()
    processAction('COOR_WD_CLOSE_MIANBAN')
    doWait(5)
    processAction('COOR_WD_TASK_MAIN')
    processAction('COOR_WD_TASK_MAIN')
    processAction('COOR_WD_RW_L2')
    processAction('COOR_WD_YOUCE_BTN1')
    doLockScreen()
    doWait(60*5)



def _doJJC():
    processAction('COOR_WD_JJC_AI')
    processAction('COOR_WD_JJC_OK')
    processAction('COOR_WD_ZIDONGZHANDOU')
    
def _doJJC5():
    for x in range(5):
        _doJJC()
        doWait(30)

def doJJC(pos='L4'):
    doWakeUp()
    doWait(5)
    processAction('COOR_WD_CLOSE_MIANBAN')
    processAction('COOR_WD_TASK_MAIN')
    processAction('COOR_WD_TASK_MAIN')
    processAction('COOR_WD_RW_'+pos)
    doWait(5)
    processAction('COOR_WD_YOUCE_BTN1')
    _doJJC5()
    processAction('COOR_WD_CLOSE_MIANBAN')
    doLockScreen()


def doAllShimen():
    for x in range(3):
        doWDLaunch(x)
        doShimen()

def doAllBangpaiRenwu():
    for x in range(3):
        doWDLaunch(x)
        doBangpaiRenwu(False)

def doAllJJC():
    for x in range(3):
        doWDLaunch(x)
        doJJC()

def doAllLoginAndCreateTeam():
    doLogin(0)
    doTeamCreate()

    doWait(2)
    doLogin(1)
    doTeamJoin()

    doWait(2)
    doLogin(2)
    doTeamJoin()

    doWait(2)
    doLogin(3)
    doTeamJoin()


    doWait(2)
    processAction('CMD_START_WD_0',6)
    #doWDLaunch(0)
    doTeamAgree()

def doShuadao():
    doWakeUp()
    doWDLaunch(0)
    processAction('COOR_WD_CLOSE_MIANBAN')
    processAction('COOR_WD_BTN_SHUADAO')
    processAction('COOR_WD_SHUADAO_FM')
    processAction('COOR_WD_YOUCE_BTN2')
    processAction('COOR_WD_SHUADAO_PIPEI')
    processAction('COOR_WD_CLOSE_MIANBAN')
    processAction('COOR_WD_BTN_SHUADAO')
    processAction('COOR_WD_SHUADAO_FM',3)
    processAction('COOR_WD_YOUCE_BTN1')
    processAction('COOR_WD_BTN_SHUADAO')
    processAction('COOR_WD_SHUADAO_FM',3)
    

def doAllLoginAndCreateTeamAndShuadao():
    doAllLoginAndCreateTeam()
    doShuadao()

def doShimenBangpaiJJC():
    doJJC()
    doSaoDang()
    doBangpaiRenwu()
    doShimen()


def doAllShimenBangpaiJJC():
    for x in range(3):
        doWDLaunch(x)
        doShimenBangpaiJJC()

def doLoginAndAllShimenBangpaiJJC(uarray = None):
    global GLOBAL_CUR_ACCOUNT
    if uarray == None:
        uarray = range(3)
    if type(uarray) == str:
        uarray = [int(uarray)]
    for x in uarray:
        GLOBAL_CUR_ACCOUNT = x
        doLogin(x)
        doQianDao()
        doShimenBangpaiJJC()



def doAllQiandao():
    for x in range(3):
        doWDLaunch(x)
        doQianDao()






def doCreateTeamAndTaskTTT():
    doNotify('######################','daole tongtianta le !!!!')
    # x = raw_input()
    doWakeUp()

    processAction('COOR_WD_CLOSE_MIANBAN')
    processAction('CMD_START_WD_1', 15)
    processAction('CMD_START_WD_2', 15)
    processAction('CMD_START_WD_0', 6)

    #doWDLaunch(0)
    doTeamCreate()

    doWait(2)
    #doWDLaunch(1)
    processAction('CMD_START_WD_1',6)
    doTeamJoin()

    doWait(2)
    #doWDLaunch(2)
    processAction('CMD_START_WD_2',6)
    doTeamJoin()

    doWait(2)
    # doWDLaunch(0)
    processAction('CMD_START_WD_0',5)
    doTeamAgree()

    doTaskTTT()
    doLockScreen()

def doTask10():
    doWakeUp()
    processAction('COOR_WD_TASK_MAIN')
    processAction('COOR_WD_RW_L3')
    processAction('COOR_WD_YOUCE_BTN1')
    processAction('COOR_WD_TASK_MAIN')
    processAction('COOR_WD_RW_L3')
    doLockScreen()
    doWait(60*40)

def doTask20():
    processAction('COOR_WD_TASK_MAIN')
    processAction('COOR_WD_RW_R1')
    processAction('COOR_WD_YOUCE_BTN1')
    processAction('COOR_WD_TASK_MAIN')
    processAction('COOR_WD_RW_R1')
    doWait(60*15)

def doJoinWith4():
    doWDLaunch(3)
    doWait(10)
    processAction('COOR_WD_AGREE')
    processAction('COOR_WD_CLOSE_MIANBAN')
    processAction('COOR_WD_FRIEND_OPEN')
    processAction('COOR_WD_FRIEND_TAB')
    processAction('COOR_WD_FRIEND_OPEN_GROUP')
    processAction('COOR_WD_FRIEND_INVITE_OPEN')
    processAction('COOR_WD_FRIEND_INVITE_JOIN')
    processAction('COOR_WD_FRIEND_OPEN_GROUP')
    processAction('COOR_WD_CLOSE_FRIEND')

    
def doAgree4():
    doWDLaunch(0)
    #processAction('COOR_WD_AGREE')
    processAction('COOR_TTT_LEAVE')
    processAction('COOR_TTT_LEAVE')
    doTeamAgree()
    
def doTask2010():
    doJoinWith4()
    doAgree4()
    doTask20()
    doTask10()


def doCreateTeamAndTaskTTTAnd2010():
    doCreateTeamAndTaskTTT()
    doWait(60*20)
    doTask2010()


def do4():
    doLoginAndAllShimenBangpaiJJC([3])
    doTaskTTTSingle()
    


def doAutoRichang(uid=None):
    doLoginAndAllShimenBangpaiJJC(uid)
    doCreateTeamAndTaskTTTAnd2010()

def doAutoRichangWith4():
    do4()
    doAutoRichang()

#####vvvvvvvvvvvvvvvvvvvvv2021年01月04日vvvvvvvvvvvvvvvvvvvvv#


def doTaskPanelRenwuJY(pos, btn='YOUCE_BTN1'):
    pos = pos.upper()
    doWakeUp()
    doWait(2)
    processAction('COOR_WD_TASK_MAIN')
    processAction('COOR_WD_TASK_MAIN')
    processAction('COOR_WD_RW_JY')
    processAction('COOR_WD_RW_'+pos)
    if(btn != ''):
        processAction('COOR_WD_'+btn, 1)

def doCreateGuDingTeam(needLogin=False):
    if needLogin:
        doLogin(0)
    else:
        doWDLaunch(0)
    doTeamCreate()

    doWait(2)
    if needLogin:
        doLogin(1)
    else:
        doWDLaunch(1)
    doTeamJoin()

    doWait(2)
    if needLogin:
        doLogin(2)
    else:
        doWDLaunch(2)
    doTeamJoin()

    doWait(2)
    if needLogin:
        doLogin(3)
    else:
        doWDLaunch(3)
    doTeamJoin()

    doWDLaunch(0)

def doAutoRichangSingle(accountId):
    # if(accountId != 1):
    doLockScreen()
    doLogin(accountId)
    #jjc
    doJJC('L4')
    processAction('COOR_WD_CLOSE_MIANBAN')
    
    #帮派任务
    doTaskPanelRenwu('L2', 'YOUCE_BTN1')
    doWait(250)
    processAction('COOR_WD_CLOSE_MIANBAN')
    
    #师门
    doTaskPanelRenwu('L1', '')
    doWait(200)
    processAction('COOR_WD_CLOSE_MIANBAN')


def doTTT2021():
    # 选择 经验 - L4
    doLockScreen()
    doWDLaunch(0)
    doTaskPanelRenwuJY('L4')
    processAction('COOR_TTT_CHOICE_EXP',1)

    doWDLaunch(1, 6)
    processAction('COOR_TTT_CHOICE_EXP',1)
    processAction('COOR_TTT_CHOICE_EXP',1)

    doWDLaunch(2, 6)
    processAction('COOR_TTT_CHOICE_EXP',1)
    processAction('COOR_TTT_CHOICE_EXP',1)

    doWDLaunch(3, 6)
    processAction('COOR_TTT_CHOICE_EXP',1)
    processAction('COOR_TTT_CHOICE_EXP',1)

    doWDLaunch(0, 6)
    processAction('COOR_TTT_CLOSE_CANCEL')
    processAction('COOR_TTT_GOFRIGHT')
    doWait(1200)
    processAction('COOR_TTT_LEAVE')

def doTaskPanelRenwuRepeat(pos, btn='YOUCE_BTN1'):
    pos = pos.upper()
    processAction('COOR_WD_TASK_MAIN')
    processAction('COOR_WD_RW_'+pos)
    if(btn != ''):
        processAction('COOR_WD_'+btn)

def do13():
    doLockScreen()
    doTaskPanelRenwu('R1')
    doTaskPanelRenwuRepeat('R1')
    doWait(300)
    doTaskPanelRenwu('L3')
    doTaskPanelRenwuRepeat('L3')
    doWait(2000)
    

def doAutoRichangNoVip():
    doLockScreen()
    doAutoRichangSingle(0)
    doAutoRichangSingle(1)
    doAutoRichangSingle(2)
    doAutoRichangSingle(3)
    doCreateGuDingTeam()

    doTTT2021()
    do13()

    doWakeUp()

# doAutoRichangNoVip()


    


#####^^^^^^^^^^^^^^^^^^^2021年01月04日^^^^^^^^^^^^^^^^^^^#

MY_FUNCTIONS={
    1 :'doInitVA',
    
    10:'doAllLoginAndCreateTeam',
    11:'doAllLoginAndCreateTeamAndShuadao',



    20:'doCreateTeamAndTaskTTTAnd2010',
    21:'doCreateTeamAndTaskTTT',
    22:'doTaskTTT',
    24:'doTask2010',
    25:'doTask10',

    30:'doLoginAndAllShimenBangpaiJJC',
    31:'doAllShimenBangpaiJJC',
    32:'doShimenBangpaiJJC',
    33:'doAllShimen',
    34:'doLoginAndAllShimenBangpaiJJC1',
    
    88:'doAutoRichangNoVip',


    90:'doAutoRichang',
    99:'doAutoRichangWith4'
}


# doMain()

if __name__ == '__main__':
    if len(sys.argv) >= 2:
        func_name = sys.argv[1]
        locals()[func_name]()
        exit()
    while True:
        print( '=======================')
        fkeys = MY_FUNCTIONS.keys()
        #fkeys.sort()
        #python3
        list(fkeys)
        for ff in fkeys:
            print( ff,'\t' ,MY_FUNCTIONS[ff])

        choice = input('Input your choice:')
        choicestr = choice
        try:
            choice = int(choice)
            # if MY_FUNCTIONS.has_key(choice):
            #python3 support
            if choice in MY_FUNCTIONS:
                print( '>>', MY_FUNCTIONS[choice])
                globals()[MY_FUNCTIONS[choice]]()

        except :
            choice = choicestr.split(' ')
            func = choice[0]
            if len(choice) == 1:
                globals()[func]()
            elif len(choice) == 2:
                doWDLaunch(choice[1])
                globals()[func](choice[1])
            elif len(choice) == 3:
                globals()[func](choice[1], choice[2])
            elif len(choice) == 4:
                globals()[func](choice[1], choice[2], choice[3])
            elif len(choice) == 5:
                globals()[func](choice[1], choice[2], choice[3], choice[4])
            elif len(choice) == 6:
                globals()[func](choice[1], choice[2], choice[3], choice[4], choice[5])
            elif len(choice) == 7:
                globals()[func](choice[1], choice[2], choice[3], choice[4], choice[5], choice[6])
        
